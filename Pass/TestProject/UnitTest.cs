using NUnit.Framework;
using ClinicPass;
using System;

namespace Test
{
    public class Tests
    {
        [Test]
        public void TestVisitorPassConstructorSet()
        {
            VisitorPass visitorPass = new VisitorPass(1, DateTime.ParseExact("2018-12-08 14:00:00", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture));

            Assert.AreEqual("08.12.2018 14:00:00", visitorPass.getDuration().ToString());
        }

        [Test]
        public void TestCheckVisitorPass()
        {
            //true
            VisitorPass visitorPass = new VisitorPass(1, DateTime.ParseExact("2018-12-15 14:00:00", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture));
            Assert.AreEqual(visitorPass.checkPass(), true);
            //false
            VisitorPass visitorPass1 = new VisitorPass(1, DateTime.ParseExact("2017-12-08 14:00:00", "yyyy-MM-dd HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture));
            Assert.AreEqual(visitorPass1.checkPass(), false);

            //exception num
            Assert.Throws<Exception>(() => {
                visitorPass = new VisitorPass(-1, DateTime.ParseExact("2017-12-08 14:00:00", "yyyy-MM-dd HH:mm:ss",
     System.Globalization.CultureInfo.InvariantCulture));
            });
        }


        [Test]
        public void TestStaffPassConstructorSet()
        {
            //true
            StaffPass staffPass = new StaffPass(1, 2001, false);
            Assert.AreEqual(2001, staffPass.getStaffId());

            //isvalid false
            Assert.AreEqual(false, staffPass.checkPass());

            //exception
            Assert.Throws<Exception>(() => { staffPass = new StaffPass(1, -2001, true); });

            //exception num
            Assert.Throws<Exception>(() => { staffPass = new StaffPass(-1, 2001, false); });
        }

        [Test]
        public void TestVisitorPassCheck()
        {
            //true
            StaffPass staffPass = new StaffPass(1, 2001, true);
            Assert.AreEqual(true, staffPass.checkPass());
        }

        [Test]
        public void TestVisitConstructor()
        {
            var before = DateTime.Now;
            Visit visit = new Visit(new StaffPass(1, 2001, true));
            var after = DateTime.Now;
            //check entry time
            Assert.That(visit.getEntryTime(), Is.InRange(before, after));
            //pass is null
            Assert.Throws<Exception>(() => { visit = new Visit(null); });
        }

        [Test]
        public void TestVisitOutputTime()
        {
            var before = DateTime.Now;
            Visit visit = new Visit(new StaffPass(1, 2001, true));
            visit.endVisit();
            var after = DateTime.Now;
            Assert.That(visit.getOutputTime(), Is.InRange(before, after));
        }

        [Test]
        public void TestVisitCheckIsFinish()
        {
            Visit visit = new Visit(new StaffPass(1, 2001, true));
            Assert.AreEqual(false, visit.checkIsFinish());

            visit.endVisit();
            Assert.AreEqual(true, visit.checkIsFinish());
        }
        [Test]
        public void TestSecondExidVisit()
        {
            Visit visit = new Visit(new StaffPass(1, 2001, true));
            visit.endVisit();

            Assert.Throws<InvalidOperationException>(visit.endVisit);
        }
    }
}