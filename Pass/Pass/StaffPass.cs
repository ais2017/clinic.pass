﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicPass
{
    public class StaffPass : Pass
    {
        private int StaffId;
        private bool IsValid;

        public StaffPass(int passNumber, int StaffId, bool IsValid) : base(passNumber)
        {
            if (StaffId > 0)
                this.StaffId = StaffId;
            else throw new Exception("StaffId is incorrect.");

            this.IsValid = IsValid;
        }

        public override bool checkPass()
        {
            return this.IsValid;
        }

        public int getStaffId()
        {
            return StaffId;
        }

        public void setValid(bool valid)
        {
            IsValid = valid;
        }
    }
}
