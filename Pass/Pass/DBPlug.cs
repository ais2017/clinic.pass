using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace ClinicPass
{
    public interface DBPlug 
    {
        void SaveVisitorPass(VisitorPass visitorPass);

        void SaveStaffPass(StaffPass staffPass);
        int GetStaffPassCount();
        StaffPass GetStaffPass(int staffId);
    }
}