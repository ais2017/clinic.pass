﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicPass
{
    public class Visit
    {
        private DateTime entryTime;
        private DateTime outputTime;
        private Pass Pass;
        private bool IsFinish = false;

        public Visit(Pass Pass)
        {
            this.entryTime = DateTime.Now;

            if (Pass != null)
                this.Pass = Pass;
            else throw new Exception("No Pass to set.");
        }
        public void endVisit()
        {
            if (!IsFinish)
            {
                outputTime = DateTime.Now;
                IsFinish = true;
            }
            else throw new InvalidOperationException("Invalid operation");
        }

        public bool checkIsFinish()
        {
            return outputTime != default(DateTime);
        }

        public DateTime getEntryTime() { return entryTime; }
        public DateTime getOutputTime() { return outputTime; }
        public Pass GetPass() { return Pass; }
    }
}
