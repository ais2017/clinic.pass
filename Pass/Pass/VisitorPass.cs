﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicPass
{
    public class VisitorPass : Pass
    {
        private DateTime Duration;

        public VisitorPass(int passNumber, int visitorId, DateTime Duration) : base(passNumber)
        {
            if (Duration != null)
                this.Duration = Duration;
            else throw new Exception("No Duration to set.");
        }

        public override bool checkPass()
        {
            return Duration > DateTime.Now;
        }

        public DateTime getDuration()
        {
            return Duration;
        }
    }
}
