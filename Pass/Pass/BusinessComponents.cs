using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicPass
{
    public class BusinessComponents
    {
        DBPlug dBPlug; 
        public BusinessComponents(DBPlug dataBasePlug) {
            this.dBPlug = dataBasePlug;
        }       
        //---Выдать временный пропуск---
        public void CreateVisitPass(int passId, int visitorId)
        {
            DateTime duration = DateTime.Today.AddDays(1);
            VisitorPass visitorPass = new VisitorPass(passId, duration);
            dBPlug.SaveVisitorPass(visitorPass);
        }

        //---Выдать пропуск сотруднику---
        public void CreateStaffPass(int StaffId)
        {
            StaffPass staffPass = new StaffPass(dBPlug.GetStaffPassCount() + 1, StaffId, true);
            dBPlug.SaveStaffPass(staffPass);
        }

        //---Деактивировать пропуск сотрудника---
        public void DeactivateStaffPass(int StaffId)
        {
            dBPlug.SaveStaffPass(GetStaffPass(StaffId).setValid(false));
        }
    }
}
