﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicPass
{
    public abstract class Pass
    {
        private int passNumber;

        public Pass(int PassNumber)
        {
            if (PassNumber > 0)
                this.passNumber = PassNumber;
            else throw new Exception("PassNumber is incorrect");
        }

        public abstract bool checkPass();

        public virtual int getPassNumber()
        {
            return passNumber;
        }
    }
}
